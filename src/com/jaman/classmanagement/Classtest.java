package com.jaman.classmanagement;
public class Classtest {

	private int idtest;
	private String daytest;
	private String cycletest;
	private String subnametest;
	private String subcodetest;
	private String tecrnametest;
	
	public Classtest() {
		
	}
	public Classtest(int idtest, String daytest, String cycletest,
			String subnametest, String subcodetest, String tecrnametest) {
		
		this.idtest = idtest;
		this.daytest = daytest;
		this.cycletest = cycletest;
		this.subnametest = subnametest;
		this.subcodetest = subcodetest;
		this.tecrnametest = tecrnametest;
	}
	public Classtest(String daytest, String cycletest, String subnametest,
			String subcodetest, String tecrnametest) {
	
		this.daytest = daytest;
		this.cycletest = cycletest;
		this.subnametest = subnametest;
		this.subcodetest = subcodetest;
		this.tecrnametest = tecrnametest;
	}
	public int getIdtest() {
		return idtest;
	}
	public void setIdtest(int idtest) {
		this.idtest = idtest;
	}
	public String getDaytest() {
		return daytest;
	}
	public void setDaytest(String daytest) {
		this.daytest = daytest;
	}
	public String getCycletest() {
		return cycletest;
	}
	public void setCycletest(String cycletest) {
		this.cycletest = cycletest;
	}
	public String getSubnametest() {
		return subnametest;
	}
	public void setSubnametest(String subnametest) {
		this.subnametest = subnametest;
	}
	public String getSubcodetest() {
		return subcodetest;
	}
	public void setSubcodetest(String subcodetest) {
		this.subcodetest = subcodetest;
	}
	public String getTecrnametest() {
		return tecrnametest;
	}
	public void setTecrnametest(String tecrnametest) {
		this.tecrnametest = tecrnametest;
	}
	@Override
	public String toString() {
		return "Classtest [idtest=" + idtest + ", day=" + daytest
				+ ", cycle=" + cycletest + ", subname=" + subnametest
				+ ", subcode=" + subcodetest + ", teachername="
				+ tecrnametest + "]";
	}
	
}
