package com.jaman.classmanagement;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class Viewactivity extends Activity {
	
	EditText etview;
	ListView listroutine;
	DatabaseHelper dbhelper;
	Customroutine adapter;
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewroutine);
        etview=(EditText)findViewById(R.id.etview); 
        listroutine=(ListView)findViewById(R.id.listroutine);
        dbhelper=new DatabaseHelper(this);
	}
	
	public void okbtn(View v)
	{
		String day=etview.getText().toString();
	    ArrayList<Routine> routines=dbhelper.getAlldata(day);
	    if(routines != null && routines.size()>0)
	       {
	    	adapter=new Customroutine(this,routines);
	    	listroutine.setAdapter(adapter);
		   }
		     
	    else if(routines.size()==0)
	       {
		     Toast.makeText(getApplicationContext(),"Data Not found",Toast.LENGTH_LONG).show();
	       }
	}

}
