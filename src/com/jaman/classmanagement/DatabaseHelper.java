package com.jaman.classmanagement;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {
	
	public static final String DB_NAME="class_management";
	public static final int DB_Version=1;
	public static final String routine_table="classroutine";
	public static final String classtest_table="classtest";
	
	public static final String ID_Field="_id";
	public static final String Day_Field="day";
	public static final String STime_Field="stime";
	public static final String ETime_Field="etime";
	public static final String SubName_Field="subname";
	public static final String SubCode_Field="subcode";
	public static final String Teacher_Field="tecrname";
	
	public static final String IDtest_Field="_idtest";
	public static final String Daytest_Field="daytest";
	public static final String Cycletest_Field="cycletest";
	public static final String SubNametest_Field="subnametest";
	public static final String SubCodetest_Field="subcodetest";
	public static final String Teachertest_Field="tecrnametest";
	
	
	public static final String Create_Table_sql="create table "+routine_table+" ("+ID_Field+
			" integer primary key, "+Day_Field+" Text, "+STime_Field+" Text, "+ETime_Field+" Text," +
					" "+SubName_Field+" Text, "+SubCode_Field+" Text, "+Teacher_Field+" Text)";
	
	public static final String Create_Table_test="create table "+classtest_table+" ("+IDtest_Field+
			" integer primary key, "+Daytest_Field+" Text, " +Cycletest_Field+" Text, "+SubNametest_Field+" Text, "
			+SubCodetest_Field+" Text, "+Teachertest_Field+" Text)";

	public DatabaseHelper(Context context) {
		super(context,DB_NAME,null,DB_Version);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(Create_Table_sql);
		Log.e("Database",Create_Table_sql);
		db.execSQL(Create_Table_test);
		Log.e("Test table", Create_Table_test);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}
	
	public long insertroutine(Routine rtn)
	{
		SQLiteDatabase db=this.getWritableDatabase();
		ContentValues values=new ContentValues();
		values.put(Day_Field, rtn.getDay());
		values.put(STime_Field, rtn.getStime());
		values.put(ETime_Field, rtn.getEtime());
		values.put(SubName_Field, rtn.getSubname());
		values.put(SubCode_Field, rtn.getSubcode());
		values.put(Teacher_Field, rtn.getTecrname());
		long inserted=db.insert(routine_table,null, values);
	    db.close();
	    return inserted;	
	}
	
	public long insertclasstest(Classtest cls)
	{
		SQLiteDatabase db=this.getWritableDatabase();
		ContentValues values=new ContentValues();
		values.put(Daytest_Field, cls.getDaytest());
		values.put(Cycletest_Field, cls.getCycletest());
		values.put(SubNametest_Field,cls.getSubnametest());
		values.put(SubCodetest_Field, cls.getSubcodetest());
		values.put(Teachertest_Field, cls.getTecrnametest());
		long inserted=db.insert(classtest_table,null, values);
	    db.close();
	    return inserted;	
	}
	
	public ArrayList<Routine> getAlldata(String days)
	{
		ArrayList<Routine> alldata=new ArrayList<Routine>();
		SQLiteDatabase db=this.getReadableDatabase();
		Cursor cursor=db.query(routine_table, null,Day_Field+"=?" ,new String[]{days}, null, null,null);
		
		if(cursor!=null && cursor.getCount()>0)
		{
			cursor.moveToFirst();
			for(int i=0;i<cursor.getCount();i++)
			{
				int id=cursor.getInt(cursor.getColumnIndex(ID_Field));
				String day=cursor.getString(cursor.getColumnIndex(Day_Field));
				String stime=cursor.getString(cursor.getColumnIndex(STime_Field));
				String etime=cursor.getString(cursor.getColumnIndex(ETime_Field));
				String subname=cursor.getString(cursor.getColumnIndex(SubName_Field));
				String subcode=cursor.getString(cursor.getColumnIndex(SubCode_Field));
				String tecrname=cursor.getString(cursor.getColumnIndex(Teacher_Field));
				Routine r=new Routine(id,day,stime,etime,subname,subcode,tecrname);
				alldata.add(r);
				cursor.moveToNext();
			}
		}
		cursor.close();
		db.close();
		return alldata;
	}
	
	public ArrayList<Classtest> getAlldatatest(String days,String cycle)
	{
		ArrayList<Classtest> alldata=new ArrayList<Classtest>();
		SQLiteDatabase db=this.getReadableDatabase();
		Cursor cursor=db.query(classtest_table, null,Daytest_Field+"=? and "+Cycletest_Field+"=?",new String[]{days,cycle}, null, null,null);
		
		if(cursor!=null && cursor.getCount()>0)
		{
			cursor.moveToFirst();
			for(int i=0;i<cursor.getCount();i++)
			{
				int idtest=cursor.getInt(cursor.getColumnIndex(IDtest_Field));
				String daytest=cursor.getString(cursor.getColumnIndex(Daytest_Field));
				String cycletest=cursor.getString(cursor.getColumnIndex(Cycletest_Field));
				//String etime=cursor.getString(cursor.getColumnIndex(ETime_Field));
				String subnametest=cursor.getString(cursor.getColumnIndex(SubNametest_Field));
				String subcodetest=cursor.getString(cursor.getColumnIndex(SubCodetest_Field));
				String tecrnametest=cursor.getString(cursor.getColumnIndex(Teachertest_Field));
				Classtest r=new Classtest(idtest,daytest,cycletest,subnametest,subcodetest,tecrnametest);
				alldata.add(r);
				cursor.moveToNext();
			}
		}
		cursor.close();
		db.close();
		return alldata;
	}

}
