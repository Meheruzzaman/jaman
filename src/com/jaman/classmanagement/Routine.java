package com.jaman.classmanagement;


public class Routine {

	private int id;
	private String day;
	private String stime;
	private String etime;
	private String subname;
	private String subcode;
	private String tecrname;
	
	public Routine() {
	}
	public Routine(int id, String day, String stime, String etime,
			String subname, String subcode, String tecrname) {
		this.id = id;
		this.day = day;
		this.stime = stime;
		this.etime = etime;
		this.subname = subname;
		this.subcode = subcode;
		this.tecrname = tecrname;
	}
	public Routine(String day, String stime, String etime, String subname,
			String subcode, String tecrname) {
		super();
		this.day = day;
		this.stime = stime;
		this.etime = etime;
		this.subname = subname;
		this.subcode = subcode;
		this.tecrname = tecrname;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getStime() {
		return stime;
	}
	public void setStime(String stime) {
		this.stime = stime;
	}
	public String getEtime() {
		return etime;
	}
	public void setEtime(String etime) {
		this.etime = etime;
	}
	public String getSubname() {
		return subname;
	}
	public void setSubname(String subname) {
		this.subname = subname;
	}
	public String getSubcode() {
		return subcode;
	}
	public void setSubcode(String subcode) {
		this.subcode = subcode;
	}
	public String getTecrname() {
		return tecrname;
	}
	public void setTecrname(String tecrname) {
		this.tecrname = tecrname;
	}
	@Override
	public String toString() {
		return "Routine [id=" + id + ", day=" + day + ", start time=" + stime
				+ ", ending time=" + etime + ", subject name=" + subname + ", subject code="
				+ subcode + ", teacher name=" + tecrname + "]";
	}
	
}
