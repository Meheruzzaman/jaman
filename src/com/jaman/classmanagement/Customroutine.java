package com.jaman.classmanagement;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class Customroutine extends ArrayAdapter<Routine> {
	

	Activity con;
	ArrayList<Routine> testlist;
	public Customroutine(Context context,ArrayList<Routine> routines) {
		super(context,R.layout.routinelist,routines);
		this.con=(Activity)context;
		this.testlist=routines;
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View v=null;
		if(convertView==null)
		{
			LayoutInflater inflater=con.getLayoutInflater();
			v=inflater.inflate(R.layout.routinelist, null);
			
			
			TextView stime=(TextView)v.findViewById(R.id.rstimelist);
			TextView subname=(TextView)v.findViewById(R.id.rsubnamelist);
			TextView subcode=(TextView)v.findViewById(R.id.rsubcodelist);
			TextView tecrname=(TextView)v.findViewById(R.id.rtecrnamelist);
			
			Routine test=testlist.get(position);
			stime.setText("Starting Time: "+test.getStime());
			subname.setText("Subject Name: "+test.getSubname());
			subcode.setText("Subject Code: "+test.getSubcode());
			tecrname.setText("Teacher Name: "+test.getTecrname());
		}else
		{
			v=convertView;
		}
		
		return v;
	}
}
