package com.jaman.classmanagement;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CustomizedAdapter extends ArrayAdapter<Classtest>{
	
	Activity con;
	ArrayList<Classtest> testlist;
	public CustomizedAdapter(Context context,ArrayList<Classtest> classtests) {
		super(context,R.layout.viewclasstestlist,classtests);
		this.con=(Activity)context;
		this.testlist=classtests;
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View v=null;
		if(convertView==null)
		{
			LayoutInflater inflater=con.getLayoutInflater();
			v=inflater.inflate(R.layout.viewclasstestlist, null);
			
			TextView subnametest=(TextView)v.findViewById(R.id.subnametest);
			TextView subcodetest=(TextView)v.findViewById(R.id.subcodetest);
			TextView tecrnametest=(TextView)v.findViewById(R.id.tecrnametest);
			
			Classtest test=testlist.get(position);
			subnametest.setText("Subject Name: "+test.getSubnametest());
			subcodetest.setText("Subject Code: "+test.getSubcodetest());
			tecrnametest.setText("Teacher Name: "+test.getTecrnametest());
		}else
		{
			v=convertView;
		}
		
		return v;
	}
}
