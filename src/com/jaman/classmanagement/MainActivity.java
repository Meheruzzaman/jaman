package com.jaman.classmanagement;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;

public class MainActivity extends Activity {

	DatabaseHelper dbhelper;
	private RadioGroup rgroutine,rgtest;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rgroutine=(RadioGroup)findViewById(R.id.rgroutine);
        rgtest=(RadioGroup)findViewById(R.id.rgtest);
    
        dbhelper=new DatabaseHelper(this);
    }
    
    public void okroutine(View v)
    {
    	int checked=rgroutine.getCheckedRadioButtonId();
    	//String days=etview.getText().toString();
    	switch (checked) {
		case R.id.rginsert:
			Intent intent1=new Intent(this,Insert.class);
			startActivity(intent1);			
			break;

         case R.id.rgview:
        	 Intent intent2=new Intent(this,Viewactivity.class);
        	 startActivity(intent2);
        	 break;
		   default:
		    	break;
		}
    }
    public void oktest(View v)
    {
    	int checked=rgtest.getCheckedRadioButtonId();
    	
    	switch (checked) {
		case R.id.rtestinsert:
			Intent intent1=new Intent(this,Inserttest.class);
			startActivity(intent1);			
			break;

         case R.id.rtestview:
        	 Intent intent2=new Intent(this,Viewclasstestlist.class);
        	 startActivity(intent2);
        	 break;
		   default:
		    	break;
		}
    }
}
