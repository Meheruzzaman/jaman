package com.jaman.classmanagement;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.jaman.classmanagement.DatabaseHelper;
import com.jaman.classmanagement.R;


public class Inserttest extends Activity {
	
	EditText etdaytest,etcycletest,etsubnametest,etsubcodetest,etteachertest;
	DatabaseHelper dbhelper;
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inserttest);
        etdaytest=(EditText)findViewById(R.id.etdaytest);
        etcycletest=(EditText)findViewById(R.id.etcycletest);
        etsubnametest=(EditText)findViewById(R.id.etsubnametest);
        etsubcodetest=(EditText)findViewById(R.id.etsubcodetest);
        etteachertest=(EditText)findViewById(R.id.etteachertest);
        dbhelper=new DatabaseHelper(this);
	}
	
	public void btntest(View v)
	{
		String daytest=etdaytest.getText().toString();
		String cycletest=etcycletest.getText().toString();
		String subnametest=etsubnametest.getText().toString();
		String subcodetest=etsubcodetest.getText().toString();
		String teachertest=etteachertest.getText().toString();
		
		Classtest classtest=new  Classtest(daytest, cycletest, subnametest, subcodetest, teachertest);
		//Toast.makeText(getApplicationContext(), classtest.toString(), Toast.LENGTH_LONG).show();

		 long inserted=dbhelper.insertclasstest(classtest);
	    	
	    	if(inserted>=0)
	    	{
	    		Toast.makeText(getApplicationContext(), "Data inserted",Toast.LENGTH_LONG).show();
	    	}else
	    	{
	    		Toast.makeText(getApplicationContext(), "Data not inserted",Toast.LENGTH_LONG).show();
	    	}
	    	finish();
		}
	}


