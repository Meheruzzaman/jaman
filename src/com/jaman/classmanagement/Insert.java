package com.jaman.classmanagement;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Insert extends Activity {

	EditText etday,etstart,etend,etsubname,etsubcode,etteacher;
	DatabaseHelper dbhelper;
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.insert);
        etday=(EditText)findViewById(R.id.etday);
        etstart=(EditText)findViewById(R.id.etstart);
        etend=(EditText)findViewById(R.id.etend);
        etsubname=(EditText)findViewById(R.id.etsubname);
        etsubcode=(EditText)findViewById(R.id.etsubcode);
        etteacher=(EditText)findViewById(R.id.etteacher);
        dbhelper=new DatabaseHelper(this);
	}
	public void save(View v)
	{
		String day=etday.getText().toString();
		String stime=etstart.getText().toString();
		String etime=etend.getText().toString();
		String subname=etsubname.getText().toString();
		String subcode=etsubcode.getText().toString();
		String teacher=etteacher.getText().toString();
		
		Routine routine=new Routine(day,stime,etime,subname,subcode,teacher);
		//Toast.makeText(getApplicationContext(), routine.toString(),Toast.LENGTH_LONG).show();
		
        long inserted=dbhelper.insertroutine(routine);
    	
    	if(inserted>=0)
    	{
    		Toast.makeText(getApplicationContext(), "Data inserted",Toast.LENGTH_LONG).show();
    	}else
    	{
    		Toast.makeText(getApplicationContext(), "Data not inserted",Toast.LENGTH_LONG).show();
    	}
    	finish();
	}
}
