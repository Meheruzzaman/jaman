package com.jaman.classmanagement;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class Viewclasstestlist extends Activity {
	

	EditText ettestcycle,ettestday;
	ListView lstestview;
	DatabaseHelper dbhelper;
	CustomizedAdapter adapter;
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewclasstest);
        ettestcycle=(EditText)findViewById(R.id.ettestcycle);
        ettestday=(EditText)findViewById(R.id.ettestday);
        lstestview=(ListView)findViewById(R.id.lstestview);
        dbhelper=new DatabaseHelper(this);
	}
	public void testview(View v)
	{
		String day=ettestday.getText().toString();
		String cycle=ettestcycle.getText().toString();
	    ArrayList<Classtest> classtests=dbhelper.getAlldatatest(day,cycle);
	    if(classtests != null && classtests.size()>0)
	    {
	    	adapter=new CustomizedAdapter(this,classtests);
	    	lstestview.setAdapter(adapter);
	    }
	    else
	    {
	    	Toast.makeText(getApplicationContext(),"Data Not Found",Toast.LENGTH_LONG).show();
	    }
	}
}
